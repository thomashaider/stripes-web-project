package emich.ilit.corpus.web.action;

import static org.uimafit.factory.AnalysisEngineFactory.createPrimitiveDescription;

import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.impl.XmiCasSerializer;
import org.apache.uima.jcas.JCas;

import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpPosTagger;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordSegmenter;
import edu.stanford.nlp.io.StringOutputStream;
import emich.ilit.corpus.dkpro.DkProDemo;
import net.sourceforge.stripes.action.*;

import org.uimafit.factory.AnalysisEngineFactory;



/**
 * Specify a uima pipeline xml descriptor.
 * @author Thomas Haider
 */
public class PipelineActionBean implements ActionBean{
    private ActionBeanContext context;

    private String inputPlaintext;
    private String outputText;
    
    private boolean segment;
    private boolean pos;
    private boolean lemma;
    private boolean constituentParser;
    private boolean dependencyParser;
    
    public ActionBeanContext getContext() {
    	return context; 
    	}
    
    public void setContext(ActionBeanContext context) {
    	this.context = context; 
    	}


    @DefaultHandler
    public Resolution send(){
    	try{
    		
			String testDocument = getInputPlaintext();
			outputText = testDocument.replace('a', 'e') + " " + isSegment();
			
			//AnalysisEngineDescription stanSeg = createPrimitiveDescription(StanfordSegmenter.class);
			AnalysisEngineDescription tagger = createPrimitiveDescription(OpenNlpPosTagger.class);
			
			AnalysisEngineDescription theTwo = AnalysisEngineFactory.createAggregateDescription(tagger);
			
			
			//AnalysisEngine oneMore = AnalysisEngineFactory.createPrimitive(theTwo, configurationData);
			AnalysisEngine engine = AnalysisEngineFactory.createAggregate(theTwo);
			
			JCas jcas = engine.newJCas();
			jcas.setDocumentLanguage("en");
			jcas.setDocumentText(testDocument);
			engine.process(jcas);
			
			/*
			JCas jcas = DkProDemo.runParserUp("en", "factored", testDocument);
	         System.out.println(jcas);
	         System.out.println(jcas.getCas());
	        
	         OutputStream outputText = new FileOutputStream("/home/tom/lala.out");
	         
	         //OutputStream output = new FileOutputStream("theXML/parser.out");
	         
	         XmiCasSerializer.serialize(jcas.getCas(), outputText);
		    */
    	}
    	catch (Exception e){
    		e.printStackTrace();
    	}
    	return new ForwardResolution("WEB-INF/jsp/Pipeline.jsp");
    }

	public String getInputPlaintext() {
		return inputPlaintext;
	}

	public void setInputPlaintext(String inputPlaintext) {
		this.inputPlaintext = inputPlaintext;
	}

	public String getOutputText() {
		return outputText;
	}

	public void setOutputText(String outputText) {
		this.outputText = outputText;
	}

	public boolean isSegment() {
		return segment;
	}

	public void setSegment(boolean segment) {
		this.segment = segment;
	}

	public boolean isPos() {
		return pos;
	}

	public void setPos(boolean pos) {
		this.pos = pos;
	}

	public boolean isLemma() {
		return lemma;
	}

	public void setLemma(boolean lemma) {
		this.lemma = lemma;
	}

	public boolean isConstituentParser() {
		return constituentParser;
	}

	public void setConstituentParser(boolean constituentParser) {
		this.constituentParser = constituentParser;
	}

	public boolean isDependencyParser() {
		return dependencyParser;
	}

	public void setDependencyParser(boolean dependencyParser) {
		this.dependencyParser = dependencyParser;
	}

}
