package emich.ilit.corpus.web.action;



import net.sourceforge.stripes.action.ActionBean;
import net.sourceforge.stripes.action.ActionBeanContext;
import net.sourceforge.stripes.action.DefaultHandler;
import net.sourceforge.stripes.action.ForwardResolution;
import net.sourceforge.stripes.action.Resolution;


public class HelloActionBean implements ActionBean {
	 private ActionBeanContext ctx;
	 public ActionBeanContext getContext() { return ctx; }
	 public void setContext(ActionBeanContext ctx) { this.ctx = ctx; }

	 private String msg;
	 private String userName;

	 @DefaultHandler
	 public Resolution sayHello() {
	  this.msg = "Hello, Stripes!";
	  return new ForwardResolution(VIEW);
	 }

	 public Resolution doSayHello() {
	  this.userName = "Hello, " + userName + "!";
	  return new ForwardResolution(VIEW);
	 }

	 public Resolution sayHelloWorld() {
		 this.msg = " Hello, World!!!";
		 return new ForwardResolution(VIEW);
	}
	 public String getMsg() { return msg; }
	 public void setMsg(String msg) { this.msg = msg; }
	 public String getUserName() {  return userName; }
	 public void setUserName(String userName) {  this.userName = userName; }
	 
	 private static final String VIEW = "/WEB-INF/jsp/hello.jsp";
}

