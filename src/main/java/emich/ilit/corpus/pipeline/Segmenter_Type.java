
/* First created by JCasGen Fri Sep 06 18:06:59 EDT 2013 */
package emich.ilit.corpus.pipeline;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Mon Sep 09 13:30:47 EDT 2013
 * @generated */
public class Segmenter_Type extends Annotation_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (Segmenter_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = Segmenter_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new Segmenter(addr, Segmenter_Type.this);
  			   Segmenter_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new Segmenter(addr, Segmenter_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = Segmenter.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("emich.ilit.corpus.pipeline.Segmenter");



  /** @generated */
  final Feature casFeat_stringRange;
  /** @generated */
  final int     casFeatCode_stringRange;
  /** @generated */ 
  public String getStringRange(int addr) {
        if (featOkTst && casFeat_stringRange == null)
      jcas.throwFeatMissing("stringRange", "emich.ilit.corpus.pipeline.Segmenter");
    return ll_cas.ll_getStringValue(addr, casFeatCode_stringRange);
  }
  /** @generated */    
  public void setStringRange(int addr, String v) {
        if (featOkTst && casFeat_stringRange == null)
      jcas.throwFeatMissing("stringRange", "emich.ilit.corpus.pipeline.Segmenter");
    ll_cas.ll_setStringValue(addr, casFeatCode_stringRange, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public Segmenter_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_stringRange = jcas.getRequiredFeatureDE(casType, "stringRange", "uima.cas.String", featOkTst);
    casFeatCode_stringRange  = (null == casFeat_stringRange) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_stringRange).getCode();

  }
}



    