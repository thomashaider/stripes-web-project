

/* First created by JCasGen Fri Sep 06 18:06:58 EDT 2013 */
package emich.ilit.corpus.pipeline;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Mon Sep 09 13:30:47 EDT 2013
 * XML source: /home/tom/workspace/txt2tei/something/src/main/java/emich/ilit/corpus/pipeline/typeSystemDescriptor.xml
 * @generated */
public class Segmenter extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(Segmenter.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected Segmenter() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public Segmenter(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public Segmenter(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public Segmenter(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
  //*--------------*
  //* Feature: stringRange

  /** getter for stringRange - gets 
   * @generated */
  public String getStringRange() {
    if (Segmenter_Type.featOkTst && ((Segmenter_Type)jcasType).casFeat_stringRange == null)
      jcasType.jcas.throwFeatMissing("stringRange", "emich.ilit.corpus.pipeline.Segmenter");
    return jcasType.ll_cas.ll_getStringValue(addr, ((Segmenter_Type)jcasType).casFeatCode_stringRange);}
    
  /** setter for stringRange - sets  
   * @generated */
  public void setStringRange(String v) {
    if (Segmenter_Type.featOkTst && ((Segmenter_Type)jcasType).casFeat_stringRange == null)
      jcasType.jcas.throwFeatMissing("stringRange", "emich.ilit.corpus.pipeline.Segmenter");
    jcasType.ll_cas.ll_setStringValue(addr, ((Segmenter_Type)jcasType).casFeatCode_stringRange, v);}    
  }

    