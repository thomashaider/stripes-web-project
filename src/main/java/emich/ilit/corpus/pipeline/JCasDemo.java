package emich.ilit.corpus.pipeline;

import static org.uimafit.factory.AnalysisEngineFactory.*;
import static org.uimafit.factory.CollectionReaderFactory.*;
import static org.uimafit.pipeline.SimplePipeline.*;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.jcas.JCas;
import org.uimafit.component.xwriter.CASDumpWriter;
import org.uimafit.pipeline.JCasIterable;
import org.uimafit.pipeline.SimplePipeline;

import de.tudarmstadt.ukp.dkpro.core.io.text.TextReader;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpPosTagger;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordNamedEntityRecognizer;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordSegmenter;
import de.tudarmstadt.ukp.dkpro.core.tokit.BreakIteratorSegmenter;

import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.factory.JCasFactory;

public class JCasDemo 
    {
	public static void main(String[] args) throws Exception
    	{

	      // Assemble and run pipeline
	         SimplePipeline.runPipeline(  
	           createDescription(TextReader.class,
	             TextReader.PARAM_PATH, "/home/tom/uimaFiles", // first command line parameter
	             TextReader.PARAM_LANGUAGE, "en", // second command line parameter
	             TextReader.PARAM_PATTERNS, new String[] {"[+]*.txt"}),
	           createPrimitiveDescription(StanfordSegmenter.class),
	           createPrimitiveDescription(OpenNlpPosTagger.class));

	         // Custom writer class used at the end of the pipeline to write results to screen
	         class Writer extends org.uimafit.component.JCasAnnotator_ImplBase {
	           public void process(JCas jcas) {
	             jcas.getDocumentText();
	           }
	         }
	         
    	}
	
}
