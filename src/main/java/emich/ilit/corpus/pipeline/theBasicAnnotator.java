package emich.ilit.corpus.pipeline;


import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.jcas.JCas;
import emich.ilit.corpus.pipeline.Segmenter;

/**
 * Example annotator that detects room numbers using 
 * Java 1.4 regular expressions.
 */
public class theBasicAnnotator extends JCasAnnotator_ImplBase {

  public void process(JCas aJCas) {
	  String docText = aJCas.getDocumentText();
	  
	  Segmenter segmenter = new Segmenter(aJCas);
	  segmenter.setBegin(0);
	  segmenter.setEnd(20);
	  segmenter.addToIndexes();
	  
	  
	  
  }
}