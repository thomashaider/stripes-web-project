
/* First created by JCasGen Thu Sep 12 13:42:37 EDT 2013 */
package emich.ilit.corpus.pipeline;

import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.cas.impl.CASImpl;
import org.apache.uima.cas.impl.FSGenerator;
import org.apache.uima.cas.FeatureStructure;
import org.apache.uima.cas.impl.TypeImpl;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.impl.FeatureImpl;
import org.apache.uima.cas.Feature;
import org.apache.uima.jcas.tcas.Annotation_Type;

/** 
 * Updated by JCasGen Thu Sep 12 14:00:54 EDT 2013
 * @generated */
public class BasicAnnotator_Type extends Annotation_Type {
  /** @generated */
  @Override
  protected FSGenerator getFSGenerator() {return fsGenerator;}
  /** @generated */
  private final FSGenerator fsGenerator = 
    new FSGenerator() {
      public FeatureStructure createFS(int addr, CASImpl cas) {
  			 if (BasicAnnotator_Type.this.useExistingInstance) {
  			   // Return eq fs instance if already created
  		     FeatureStructure fs = BasicAnnotator_Type.this.jcas.getJfsFromCaddr(addr);
  		     if (null == fs) {
  		       fs = new BasicAnnotator(addr, BasicAnnotator_Type.this);
  			   BasicAnnotator_Type.this.jcas.putJfsFromCaddr(addr, fs);
  			   return fs;
  		     }
  		     return fs;
        } else return new BasicAnnotator(addr, BasicAnnotator_Type.this);
  	  }
    };
  /** @generated */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = BasicAnnotator.typeIndexID;
  /** @generated 
     @modifiable */
  @SuppressWarnings ("hiding")
  public final static boolean featOkTst = JCasRegistry.getFeatOkTst("emich.ilit.corpus.pipeline.BasicAnnotator");
 
  /** @generated */
  final Feature casFeat_someRandomString;
  /** @generated */
  final int     casFeatCode_someRandomString;
  /** @generated */ 
  public String getSomeRandomString(int addr) {
        if (featOkTst && casFeat_someRandomString == null)
      jcas.throwFeatMissing("someRandomString", "emich.ilit.corpus.pipeline.BasicAnnotator");
    return ll_cas.ll_getStringValue(addr, casFeatCode_someRandomString);
  }
  /** @generated */    
  public void setSomeRandomString(int addr, String v) {
        if (featOkTst && casFeat_someRandomString == null)
      jcas.throwFeatMissing("someRandomString", "emich.ilit.corpus.pipeline.BasicAnnotator");
    ll_cas.ll_setStringValue(addr, casFeatCode_someRandomString, v);}
    
  



  /** initialize variables to correspond with Cas Type and Features
	* @generated */
  public BasicAnnotator_Type(JCas jcas, Type casType) {
    super(jcas, casType);
    casImpl.getFSClassRegistry().addGeneratorForType((TypeImpl)this.casType, getFSGenerator());

 
    casFeat_someRandomString = jcas.getRequiredFeatureDE(casType, "someRandomString", "uima.cas.String", featOkTst);
    casFeatCode_someRandomString  = (null == casFeat_someRandomString) ? JCas.INVALID_FEATURE_CODE : ((FeatureImpl)casFeat_someRandomString).getCode();

  }
}



    