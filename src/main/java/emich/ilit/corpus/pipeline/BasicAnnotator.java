

/* First created by JCasGen Thu Sep 12 13:48:25 EDT 2013 */
package emich.ilit.corpus.pipeline;

import org.apache.uima.jcas.JCas; 
import org.apache.uima.jcas.JCasRegistry;
import org.apache.uima.jcas.cas.TOP_Type;

import org.apache.uima.jcas.tcas.Annotation;


/** 
 * Updated by JCasGen Thu Sep 12 14:00:54 EDT 2013
 * XML source: /home/tom/workspace/txt2tei/something/src/main/java/emich/ilit/corpus/pipeline/basicAeDescriptor.xml
 * @generated */
public class BasicAnnotator extends Annotation {
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int typeIndexID = JCasRegistry.register(BasicAnnotator.class);
  /** @generated
   * @ordered 
   */
  @SuppressWarnings ("hiding")
  public final static int type = typeIndexID;
  /** @generated  */
  @Override
  public              int getTypeIndexID() {return typeIndexID;}
 
  /** Never called.  Disable default constructor
   * @generated */
  protected BasicAnnotator() {/* intentionally empty block */}
    
  /** Internal - constructor used by generator 
   * @generated */
  public BasicAnnotator(int addr, TOP_Type type) {
    super(addr, type);
    readObject();
  }
  
  /** @generated */
  public BasicAnnotator(JCas jcas) {
    super(jcas);
    readObject();   
  } 

  /** @generated */  
  public BasicAnnotator(JCas jcas, int begin, int end) {
    super(jcas);
    setBegin(begin);
    setEnd(end);
    readObject();
  }   

  /** <!-- begin-user-doc -->
    * Write your own initialization here
    * <!-- end-user-doc -->
  @generated modifiable */
  private void readObject() {/*default - does nothing empty block */}
     
 
    
  //*--------------*
  //* Feature: someRandomString

  /** getter for someRandomString - gets 
   * @generated */
  public String getSomeRandomString() {
    if (BasicAnnotator_Type.featOkTst && ((BasicAnnotator_Type)jcasType).casFeat_someRandomString == null)
      jcasType.jcas.throwFeatMissing("someRandomString", "emich.ilit.corpus.pipeline.BasicAnnotator");
    return jcasType.ll_cas.ll_getStringValue(addr, ((BasicAnnotator_Type)jcasType).casFeatCode_someRandomString);}
    
  /** setter for someRandomString - sets  
   * @generated */
  public void setSomeRandomString(String v) {
    if (BasicAnnotator_Type.featOkTst && ((BasicAnnotator_Type)jcasType).casFeat_someRandomString == null)
      jcasType.jcas.throwFeatMissing("someRandomString", "emich.ilit.corpus.pipeline.BasicAnnotator");
    jcasType.ll_cas.ll_setStringValue(addr, ((BasicAnnotator_Type)jcasType).casFeatCode_someRandomString, v);}    
  }

    