package emich.ilit.corpus.dkpro;

import static de.tudarmstadt.ukp.dkpro.core.api.io.ResourceCollectionReaderBase.INCLUDE_PREFIX;
import static org.uimafit.factory.AnalysisEngineFactory.createAggregateDescription;
import static org.uimafit.factory.AnalysisEngineFactory.createPrimitiveDescription;
import static org.uimafit.factory.CollectionReaderFactory.createCollectionReader;

import java.io.IOException;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.resource.ResourceInitializationException;
import org.uimafit.pipeline.SimplePipeline;

import de.tudarmstadt.ukp.dkpro.core.api.resources.DKProContext;
import de.tudarmstadt.ukp.dkpro.core.io.tei.TEIReader;

/**
 * @author zesch
 *
 */
public class ThroughputTest {

    public void testAggregate(CollectionReader reader, AnalysisEngineDescription aggr)
        throws IOException, UIMAException
    {

       
        AnalysisEngineDescription timerAggregate = createAggregateDescription(
               
                aggr
             
        );
       
        SimplePipeline.runPipeline(
                reader,
                timerAggregate
        );
    }
   
    public static CollectionReader getStandardReader(String languageCode) throws IOException, ResourceInitializationException {
        if (languageCode.equals("en")) {
            String brownPath = DKProContext.getContext().getWorkspace("toolbox_corpora").getAbsolutePath() + "/brown_tei/";    
           
            return createCollectionReader(
                    TEIReader.class,
                    TEIReader.PARAM_LANGUAGE, "en",
                    TEIReader.PARAM_PATH, brownPath,
                    TEIReader.PARAM_PATTERNS, new String[] {INCLUDE_PREFIX + "*.xml.gz"}
            );
        }
        else {
            throw new IllegalArgumentException("No standard reader available for language code: " + languageCode);
        }
    }
   
}
