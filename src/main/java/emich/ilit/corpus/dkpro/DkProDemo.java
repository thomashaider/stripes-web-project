package emich.ilit.corpus.dkpro;

import static org.uimafit.factory.AnalysisEngineFactory.*;
import static org.uimafit.factory.CollectionReaderFactory.*;
import static org.uimafit.pipeline.SimplePipeline.*;

import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.impl.XmiCasSerializer;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.jcas.JCas;
import org.uimafit.component.xwriter.CASDumpWriter;
import org.uimafit.pipeline.JCasIterable;
import org.uimafit.component.xwriter.XWriter;

import de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS;
import de.tudarmstadt.ukp.dkpro.core.api.resources.DKProContext;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Lemma;
import de.tudarmstadt.ukp.dkpro.core.io.text.TextReader;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpPosTagger;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordNamedEntityRecognizer;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordSegmenter;
import de.tudarmstadt.ukp.dkpro.core.tokit.BreakIteratorSegmenter;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordLemmatizer;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordParser;

import org.uimafit.component.JCasAnnotator_ImplBase;

import de.tudarmstadt.ukp.dkpro.core.io.tei.TEIReader;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiWriter;
import de.tudarmstadt.ukp.dkpro.core.io.xml.*; 
import de.tudarmstadt.ukp.dkpro.core.testing.AssertAnnotations;
import de.tudarmstadt.ukp.dkpro.core.testing.TestRunner;
import edu.stanford.nlp.io.StringOutputStream;

import org.apache.uima.util.XmlCasSerializer;

public class DkProDemo 
    {
	public static void main(String[] args) throws Exception
    	{

			 //read a collection of Files from dir
	         CollectionReader cr = createCollectionReader(
	            TextReader.class,
	            TextReader.PARAM_PATH, "/home/tom/uimaFiles",
	            TextReader.PARAM_LANGUAGE, "en",
	            TextReader.PARAM_PATTERNS, new String[] {"[+]*.txt"});
	           
	         //create primitive descriptions of pipeline stages
	         AnalysisEngineDescription seg = createPrimitiveDescription(BreakIteratorSegmenter.class);
	         AnalysisEngineDescription stanSeg = createPrimitiveDescription(StanfordSegmenter.class);
	         AnalysisEngineDescription tagger = createPrimitiveDescription(OpenNlpPosTagger.class);
	         AnalysisEngineDescription lemma = createPrimitiveDescription(StanfordLemmatizer.class);
	         AnalysisEngineDescription ner = createPrimitiveDescription(StanfordNamedEntityRecognizer.class);
	 		 AnalysisEngineDescription parser = createPrimitiveDescription(StanfordParser.class,
					StanfordParser.PARAM_VARIANT, "factored",
					StanfordParser.PARAM_PRINT_TAGSET, true,
					StanfordParser.PARAM_CREATE_CONSTITUENT_TAGS, true,
					StanfordParser.PARAM_CREATE_DEPENDENCY_TAGS, true,
					StanfordParser.PARAM_CREATE_PENN_TREE_STRING, true,
					StanfordParser.PARAM_CREATE_POS_TAGS, true
					);
	 		 
	        
	 		//String brownPath = DKProContext.getContext().getWorkspace("toolbox_corpora").getAbsolutePath() + "/brown_tei/"; 
	 		 
	 		CollectionReader teireader = createCollectionReader(
	 				TEIReader.class,
	 				TEIReader.PARAM_LANGUAGE, "en",
	 				TEIReader.PARAM_PATH, "/home/tom/Documents/TEI/corpus",
	 				
	 				TEIReader.PARAM_WRITE_SENTENCES, true,
	 				TEIReader.PARAM_WRITE_TOKENS, true,
	 				TEIReader.PARAM_WRITE_POS, true,
	 				TEIReader.PARAM_WRITE_LEMMA, true,
	 				
	 				TEIReader.PARAM_PATTERNS, new String[] {"[+]" + "*.xml"}
	 				);
	 		
	 		 
	 		//a simple CAS dump writer
	 		AnalysisEngineDescription caswriter = createPrimitiveDescription(
	 				CASDumpWriter.class,
	 			    CASDumpWriter.PARAM_OUTPUT_FILE, "theXML/cas_dump.txt");
	 		 
	 		 //an inline xml writer with xslt transformation
	         AnalysisEngineDescription xml = createPrimitiveDescription(
	        	XmlWriterInline.class,
	        	XmlWriterInline.PARAM_XSLT, "uima.xsl",
	        	XmlWriterInline.PARAM_STRIP_EXTENSION, true,
	            XmlWriterInline.PARAM_PATH, "theXML/xmlWithXslt"	      
	        		 );
	         
	         //inline xml without xslt
	         AnalysisEngineDescription xml2 = createPrimitiveDescription(
	 	        	XmlWriterInline.class,
	 	            XmlWriterInline.PARAM_PATH, "theXML/xmlWoXslt"	      
	 	        		 );
	         
	         //xmi objects
	         AnalysisEngineDescription xmi = createPrimitiveDescription(
	 	        	XmiWriter.class,
	 	        	XmiWriter.PARAM_STRIP_EXTENSION, true,
	 	            XmiWriter.PARAM_PATH, "theXML/xmi"	      
	 	        		 );
	         
	         //run the actual pipeline of descriptions and writers
	         runPipeline(cr,stanSeg,tagger,lemma,ner,parser,caswriter,xml,xml2,xmi);
	         runPipeline(teireader,tagger);
	         System.out.println("Done");
	         
	         //
	         //Parser section
	         //
	         
	         String testDocument = "Michigan Governor Rick Snyder has said Detroit may have hit rock bottom with its bankruptcy filing, "
	         		+ "but the move will reverse decades of decay.";
	         
	         JCas jcas = runParserUp("en", "factored", testDocument);
	         System.out.println(jcas);
	         System.out.println(jcas.getCas());
	         
	         OutputStream someOutput = new StringOutputStream();
	         
	         OutputStream output = new FileOutputStream("theXML/parser.out");
	         
	         XmiCasSerializer.serialize(jcas.getCas(), output);
	         
    	} 

	
	
	public static JCas runParserUp(String aLanguage, String aVariant, String aText)
		throws Exception
	{
		AnalysisEngineDescription segmenter = createPrimitiveDescription(StanfordSegmenter.class);
		
		//optional
		AnalysisEngineDescription tagger = createPrimitiveDescription(OpenNlpPosTagger.class);

		// setup English
		AnalysisEngineDescription parser = createPrimitiveDescription(StanfordParser.class,
				StanfordParser.PARAM_VARIANT, aVariant,
				StanfordParser.PARAM_PRINT_TAGSET, true,
				StanfordParser.PARAM_CREATE_CONSTITUENT_TAGS, true,
				StanfordParser.PARAM_CREATE_DEPENDENCY_TAGS, true,
				StanfordParser.PARAM_CREATE_PENN_TREE_STRING, true,
				StanfordParser.PARAM_CREATE_POS_TAGS, true);

		AnalysisEngineDescription aggregate = createAggregateDescription(segmenter, tagger, parser);
		
		AnalysisEngine engine = createPrimitive(aggregate);
		JCas jcas = engine.newJCas();
		jcas.setDocumentLanguage(aLanguage);
		jcas.setDocumentText(aText);
		engine.process(jcas);
		
		return jcas;
	}
}
	  
