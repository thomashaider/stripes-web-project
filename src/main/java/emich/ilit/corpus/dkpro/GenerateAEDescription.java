package emich.ilit.corpus.dkpro;


	import static org.uimafit.factory.AnalysisEngineFactory.*;
import static org.uimafit.factory.CollectionReaderFactory.*;
import static org.uimafit.pipeline.SimplePipeline.*;

	import java.io.FileOutputStream;
import java.io.OutputStream;

	import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.impl.XmiCasSerializer;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.jcas.JCas;
import org.uimafit.component.xwriter.CASDumpWriter;
import org.uimafit.pipeline.JCasIterable;
import org.uimafit.component.xwriter.XWriter;

	import de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS;
import de.tudarmstadt.ukp.dkpro.core.api.resources.DKProContext;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Lemma;
import de.tudarmstadt.ukp.dkpro.core.io.text.TextReader;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpPosTagger;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordNamedEntityRecognizer;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordSegmenter;
import de.tudarmstadt.ukp.dkpro.core.tokit.BreakIteratorSegmenter;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordLemmatizer;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordParser;

	import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.factory.AnalysisEngineFactory;

	import de.tudarmstadt.ukp.dkpro.core.io.tei.TEIReader;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiWriter;
import de.tudarmstadt.ukp.dkpro.core.io.xml.*; 
import de.tudarmstadt.ukp.dkpro.core.testing.AssertAnnotations;
import de.tudarmstadt.ukp.dkpro.core.testing.TestRunner;
import edu.stanford.nlp.io.StringOutputStream;

import org.apache.uima.util.XmlCasSerializer;

	public class GenerateAEDescription {
	    
		public static void main(String[] args) throws Exception
	    	{
			AnalysisEngineDescription analysisEngine = 
				    AnalysisEngineFactory.createPrimitiveDescription(StanfordSegmenter.class
				    		);
			
				analysisEngine.toXML(new FileOutputStream("/home/tom/Documents/StanfordSegmenter.xml"));
				System.out.println("Done");
	    	}
	    }
