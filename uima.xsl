<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0" xmlns:tei="http://www.tei-c.org/ns/1.0" exclude-result-prefixes="tei">
    
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>
    <xsl:strip-space elements="*"/>
    
    
    <xsl:template match="Document">
        
        <TEI xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:apply-templates/>
        </TEI>
    </xsl:template>
    
    <xsl:template match="de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData">
        <teiHeader xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:attribute name="xml:lang">
                <xsl:value-of select="@language"/>
            </xsl:attribute>
           <fileDesc>
               <titleStmt>
                   <title></title>
               </titleStmt>
               <publicationStmt></publicationStmt>
               <sourceDesc></sourceDesc>
           </fileDesc>
        </teiHeader>
        <text xmlns="http://www.tei-c.org/ns/1.0">
            <body>
                <div>
                    <p>
                        <xsl:apply-templates/>
                    </p>
                </div>
            </body>
            
        </text>
        
    </xsl:template>
    
    <xsl:template match="de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence">
        <s xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:for-each select="descendant::de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token">
                <xsl:variable name="pos-att-token">
                    <xsl:value-of select="./@pos"/>
                </xsl:variable>
                <xsl:variable name="single-quote">
                    <xsl:value-of select="&quot;&apos;&quot;"/>
                </xsl:variable>
                <xsl:variable name="double-quote">
                    <xsl:value-of select="'&quot;'"/>
                </xsl:variable>
                <xsl:choose>
                    <xsl:when test="parent::de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.PUNC or parent::de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.O=$single-quote or parent::de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.O=$double-quote">
                        <c>
                            <xsl:attribute name="type">
                                <xsl:value-of select="normalize-space(substring-before($pos-att-token, '['))"/>
                            </xsl:attribute>
                            <xsl:value-of select="self::node()"/>
                        </c>
                    </xsl:when>
                    <xsl:otherwise>
                        <w>
                            <xsl:attribute name="type">
                                <xsl:value-of select="normalize-space(substring-before($pos-att-token, '['))"/>
                            </xsl:attribute>
                            <xsl:value-of select="self::node()"/>
                        </w>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </s>
    </xsl:template>
    
    

    <xsl:template match="*">
        <xsl:copy-of select="self::node()"/>
    </xsl:template>      
</xsl:stylesheet>